
# Welcome to your CDK Python project!

This is a blank project for CDK development with Python.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

This project is set up like a standard Python project.  The initialization
process also creates a virtualenv within this project, stored under the `.venv`
directory.  To create the virtualenv it assumes that there is a `python3`
(or `python` for Windows) executable in your path with access to the `venv`
package. If for any reason the automatic creation of the virtualenv fails,
you can create the virtualenv manually.

To manually create a virtualenv on MacOS and Linux:

```
$ python3 -m venv .venv
```

After the init process completes and the virtualenv is created, you can use the following
step to activate your virtualenv.

```
$ source .venv/bin/activate
```

If you are a Windows platform, you would activate the virtualenv like this:

```
% .venv\Scripts\activate.bat
```

Once the virtualenv is activated, you can install the required dependencies.

```
$ pip install -r requirements.txt
```

At this point you can now synthesize the CloudFormation template for this code.

```
$ cdk synth
```

To add additional dependencies, for example other CDK libraries, just add
them to your `setup.py` file and rerun the `pip install -r requirements.txt`
command.

## Useful commands

 * `cdk ls`          list all stacks in the app
 * `cdk synth`       emits the synthesized CloudFormation template
 * `cdk deploy`      deploy this stack to your default AWS account/region
 * `cdk diff`        compare deployed stack with current state
 * `cdk docs`        open CDK documentation

## Create the S3 bucket using CodeWhisperer:

![Alt text](image.png)

all I did was to write the comment: "Create an S3 Bucket using CDK with encryption", then AI helped finish the code part.

`Attributes:` including versioning, encryption, block pulic access, removal policy and auto delete. 

## Upload file to the bucket:

May need to add some policies for uploading:

1. Log into the IAM console
2. step to the Roles Page
3. click the created bucket role
4. add the PutObject policy 

![Alt text](image-1.png)

Here is my Python Script to upload a csv file to the bucket:

![Alt text](image-2.png)

It was also created by CodeWhisperer


## Result:

![Alt text](image-4.png)

![Alt text](image-3.png)