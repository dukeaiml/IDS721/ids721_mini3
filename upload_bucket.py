# upload my csv file to the bucket
import boto3

def upload_file(file_name, bucket_name):
    # Create an S3 client
    s3 = boto3.client('s3')

    try:
        # Uploads the file to the S3 bucket
        response = s3.upload_file(file_name, bucket_name, file_name)
        print(f'{file_name} uploaded successfully to {bucket_name}')
    except Exception as e:
        print(f'Error uploading {file_name} to {bucket_name}: {e}')

if __name__ == "__main__":
    file_name = 'salary_prediction_data.csv'  # Replace 'your_file.csv' with the path to your CSV file
    bucket_name = 'ids721mini3stack-myfirstbucketb8884501-pqvm59mdi84i'  # Replace 'MyFirstBucket' with your S3 bucket name
    upload_file(file_name, bucket_name)