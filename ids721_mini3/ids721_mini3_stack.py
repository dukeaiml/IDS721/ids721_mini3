from aws_cdk import (
    Stack,
    RemovalPolicy,
)
from constructs import Construct
import aws_cdk as cdk
import aws_cdk.aws_s3 as s3
import aws_cdk.aws_iam as iam
class Ids721Mini3Stack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        #Create an S3 Bucket using CDK with encryption
        bucket = s3.Bucket(self, "MyFirstBucket", versioned=True,
                           encryption=s3.BucketEncryption.KMS_MANAGED,
                           block_public_access=s3.BlockPublicAccess.BLOCK_ALL,
                           removal_policy=cdk.RemovalPolicy.DESTROY,
                           auto_delete_objects=True
                           )
        

        
        # The code that defines your stack goes here

        # example resource
        # queue = sqs.Queue(
        #     self, "Ids721Mini3Queue",
        #     visibility_timeout=Duration.seconds(300),
        # )
        
        #   Add bucket properties like versioning and encryption
            
      
 